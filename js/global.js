$(document).ready(function(){
		$('.menu-canvas > li').each(function(){
			var id = $(this).data('canvas');
			var text = $(this).children('a').children('span').text();
			var width = $(this).children('a').children('span').width();
			drawCanvas(id,text,width)
		});
		/************/
		$('.block-promotion li a').hover(function(){
			var img = $(this).data('img');
			$('.thumbnail-frame .thumbnail').attr('src',img);	
		},function(){});
		/************/
		$(".block-list-item ul ,.content-list ul").mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
			theme:"dark-thin"
		});
		/***************************/
		$(".select-style").select2({
	        allowClear: true,
	    });
		/***************************/
		$('.date').datepicker({

		})	
		/***************************/
		$('.slider .list-content').nivoSlider({
			directionNav:false,
			controlNav:false
		});
		/***************************/
		$('.start-animate').each(function(){
			var a = $(this).index()/10;
			var a = Math.random() * 2 + $(this).index()/10;
			$(this).css({
				"animation-delay":a+'s',
				"-moz-animation-delay":a+'s',
				"-o-animation-delay":a+'s',
				"-webkit-animation-delay":a+'s'
			})
		})
		/***************************/
	});
function drawCanvas(canvas,text,width) {
	var canvas = document.getElementById(canvas);
	canvas.width = width
	var text = text;
	var context = canvas.getContext('2d');
	// Gradient Text Fill and Stroke
	var gradient=context.createLinearGradient(0,0,0,40);

	gradient.addColorStop(0.5,"#fdfce5");
	gradient.addColorStop(0.8,"#fff4c5");
	gradient.addColorStop(1,"#e2c164 ");

	context.fillStyle=gradient;
	context.font="normal 20px EB Garamond";
	context.fillText(text, 0, 39);
	context.fillStyle='#FFFFFF';
	context.strokeStyle=gradient;
}