<?php
	$webroot = "./thaotay/hair/";
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv=Content-Type content ="text/html; charset=utf-8"> 
	<title>THẢO TÂY GROUP – ĐỊA CHỈ LÀM ĐẸP LÝ TƯỞNG</title>
	
	
	<meta name="description" content="THẢO TÂY GROUP được biết tới với các dịch vụ nổi tiếng như nối tóc Elastic Silk, Viện Thẩm Mỹ THẢO TÂY, Học Viện Tóc THẢO TÂY..." />
	<meta name="keyword" content="" />
	
	
	<link href='https://fonts.googleapis.com/css?family=Cormorant+SC:500&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	
	<script type='text/javascript' src="js/thaotay.js"></script>
	
	<style>
		.font{
			font-family: "Cormorant SC",serif;
		}
	</style>
	
</head>
<body>
<div class="header_bg">
<div class="bg_2_cot">
	<div id='wrapper'>
		&nbsp;
		<!-- Header -->
		<img src='img/saolaplanh.png' class="start-animate star_1"/>
		<!-- Sao Cột trái -->
		<img src='img/saolaplanh.png' class="start-animate star_2" />
		<img src='img/saolaplanh.png' class="start-animate star_3" />
		<!-- Sao cột phải -->
		<img src='img/saolaplanh.png' class="start-animate star_4" />
		<img src='img/saolaplanh.png' class="start-animate star_5" />
		
		<!-- Sao 3 dịch vụ-->
		<img src='img/saolaplanh.png' class="start-animate star_6" />
		<img src='img/saolaplanh.png' class="start-animate star_7" />
		<img src='img/saolaplanh.png' class="start-animate star_8" />
		
		<div id='content'>
		
			<div class='row'>
				
				<div id='logo_ThaoTay' class="text_center">
					<img src='img/saolaplanh.png' alt="" class="start-animate" id="sao_logo"/>
					<!--<img src='img/logo_Thaotay.png' id="img_logo_ThaoTay" alt="logo Thảo Tây"/>-->
					<img src='img/logo-thaotay-full.png' id="img_logo_ThaoTay" alt="Thảo Tây Group"/>
				</div>
			</div>
			<div class="row">
				<div id="welcome_text">
				<!-- Sao welcome text -->
				<img src='img/saolaplanh.png' class="start-animate star_9" />
				<img src='img/saolaplanh.png' class="start-animate star_10" />
				<img src='img/saolaplanh.png' class="start-animate star_11" />
				<!-- End Sao welcome text -->
				<div id="animationSandbox" class="fadeIn animated">
					
					<div id="thaotaygroup">
						
							<img src='img/Thaotaygroup.png' alt="Thảo Tây group" class="site__title mega"/>
							
					</div>
					<div class="space">&nbsp;</div>
					<div id="kinhchaoquykhach">
						<img src='img/kinhchaoquykhach.png' alt="Kính chào quý khách"/>
					</div>
					<br class='clearBoth' />
				</div>
				</div>
			</div>
			<div class="row">
				<div class="hr text_center width100">
					<img src='img/hr.png' alt=""/>
				</div>
				<br class="clearBoth" />
			</div>
			
			<div class="row">
				<div class="service_spacing_left">
					&nbsp;
				</div>
				
				<div class="services">
					
					<div class="box_1">
						<div id="noitoc">
							<a href="http://www.noitocthaotay.com">
								<img src="img/media/service_1.png" alt="Nối tóc Thảo Tây" title="Nối tóc Thảo Tây" />
							</a>
						</div>
						<div id="text_noitoc" class="text_link">
							<a href="http://www.noitocthaotay.com">
							<!--<img src="img/media/noitoc.png" alt="Nối Tóc" width="190" height="50" alt="Nối tóc Thảo Tây" title="Nối tóc Thảo Tây"/>--> 
							<span class="font" style='font-size:32px;'>HAIR SALON</span>
								<br /><span class="font" style='font-size:18px;color:#eada8f;'>NỐI TÓC</span>
							</a>
						</div>
					</div>
					
					<div class="box_1">
						<div id="spa">
						
							<a href="http://www.vienthammythaotay.com"><img src="img/media/service_2.png" width="291" height="219" alt="Spa & Clinic" title="Spa & Clinic"/></a>
						</div>
						<div  id="text_spa" style="height:auto;text-align:center">
							<a href="http://www.vienthammythaotay.com"><img src="img/media/spa.png" width="215" height="58"  alt="Spa & Clinic" title="Spa & Clinic"/></a>
						</div>
					</div>
					
					<div class="box_1">
						<div id="hocvien">
							<a href="http://www.hocvientocquoctethaotay.edu.vn"><img src="img/media/service_3.png" width="291" height="219" alt="Học viện tóc Thảo Tây" title="Học viện tóc Thảo Tây"/></a>
						</div>
						<div  id="text_hocvien" style="height:auto;text-align:center">
							<a href="http://www.hocvientocquoctethaotay.edu.vn"><img src="img/media/hocvien.png" width="153" height="57" alt="Học viện tóc Thảo Tây" title="Học viện tóc Thảo Tây"/> </a>
						</div>
					</div>
					<br class="clearBoth" />
				</div>
				<br class="clearBoth" />
			</div>
			
		
	</div>
		<!-- End Header -->
			<!-- Footer -->
		
		
	</div>
</div><!-- 2 cot -->
</div>
<div class='footer'>
&nbsp;<br />
</div>
<script>
<!-- GA -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56010070-1', 'auto');
  ga('send', 'pageview');
<!-- End GA -->

</script>
</body>
</html>